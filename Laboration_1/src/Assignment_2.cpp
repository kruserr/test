#include <math.h> // Added for more accurate PI
#include <iostream> // Added io stream
#include <iomanip> // Added For setprecision

using namespace std; // Added standard namespace

int main() {

    // Variables and constants
    double radius, circumference, area; // Changed to double to get more accurate numbers
    //const double PI = 3.14; Changed constant to a more accurate PI number M_PI

    // Input the circles radius
    cout << "Assign the circle's radius: ";
    cin >> radius; // Syntax error. Changed "=" to ">>"

    // Algorithm to calculate circumference (2*PI*r) and area (PI*r*r)
    circumference = 2 * M_PI * radius; // logical error. Corrected formulas and added more accurate PI
    area = M_PI * radius * radius; // logical error. Corrected formulas and added more accurate PI

    // Output of results
    cout << "A circle with the radius " << fixed << setprecision(2) << radius << " has the circumference "<< circumference << " and area " << area << endl; // Changed Cout over two lines to one line. and changed precision to 2 decimals

    // Validate x
    int x;
    cout << "Enter a number to validate: "; // Added text to ask user for input
    cin >> x;

    if(x == 100) { // Added starting bracket and changed "x = 100" to "x == 100"

        cout << x << " is equal to 100" << endl; // Changed to write number that is x instead of just the letter x

    } // Added ending bracket

    if(x > 0) { // Added starting bracket and removed ";"

        cout << x << " is larger than zero" << endl; // Changed to write number that is x instead of just x

    } // Added ending bracket

    // Assuming the switch statement wants to only paste when the contition is actually met, added break;
    switch(x) {

        case 5 :

            cout << x << " is equal to 5 " << endl; // Changed to write number that is x instead of just x
            break; // Added break

        case 10 :

            cout << x << " is equal to 10" << endl; // Changed to write number that is x instead of just x
            break; // Added break

        default :

            cout << x << " is neither 5 nor 10" << endl; // Changed to write number that is x instead of just x
            break; // Added break

    } // Added ending bracket

    return 0;

} // End main
