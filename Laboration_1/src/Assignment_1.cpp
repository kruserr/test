#include <iostream> // Standard
#include <iomanip> // For setw and setprecision

using namespace std; // Standard

void clear_console();
void render_starting_message();
double input_mileage_previous();
double input_mileage_current();
double input_fuel_volume();
double calculate_fuel_consumption();
void render_output();

// Cross Platform clear console
#ifdef unix

void clear_console() {

    system("clear");

}

#elif _WIN32

void clear_console() {

    system("cls");

}

#else

void clear_console() { }

#endif

// Output starting message
void render_starting_message() {

    cout << "______________________________" << endl << endl;
    cout << "Assignment 1: Fuel consumption" << endl;
    cout << "______________________________" << endl << endl;

}

// Returns user input Milage Previous
double input_mileage_previous() {

    double mileage_previous_temp;

    cout << "Fuel meter of previous refueling " << setw(0) << "[KM] " << setw(9) << ": " << setw(3) << "";
    cin >> mileage_previous_temp;

    return mileage_previous_temp;

}

// Returns user input Milage Current
double input_mileage_current() {

    double mileage_current_temp;

    cout << "Fuel meter of current refueling " << setw(6) << "[KM] " << setw(9) << ": " << setw(3) << "";
    cin >> mileage_current_temp;

    return mileage_current_temp;

}

// Returns user input Fuel Volume
double input_fuel_volume() {

    double fuel_volume_temp;

    cout << "Total volume refuled " << setw(16) << "[L] " << setw(10) << ": " << setw(3) << "";
    cin >> fuel_volume_temp;
    cout << endl;

    return fuel_volume_temp;

}

// Returns Fuel Consumption in L/Mile
double calculate_fuel_consumption() {

    const double convert_km_to_mile = 10.00; // 1 Swedish Mile equals 10 KM
    
    double mileage_previous = input_mileage_previous();
    double mileage_current = input_mileage_current();
    double fuel_volume = input_fuel_volume();
    
    double mileage = ( mileage_current - mileage_previous ) / convert_km_to_mile;

    return fuel_volume / mileage;

}

// Renders Fuel Consumption and Fuel Cost
void render_output() {
    
    const double sek_pr_liter = 14.95; // ( 2018-11 ) https://spbi.se/statistik/priser/

    double fuel_consumption = calculate_fuel_consumption();

    double fuel_cost = fuel_consumption * sek_pr_liter; // Calculates Fuel Cost, by adding SEK/L to L/Mile to get SEK/Mile

    cout << "Fuel Consumption " << setw(25) << "[L/Mile] " << setw(5) << ": " << setw(4) << "" << fixed << setprecision(2) << fuel_consumption << endl;

    cout << "Fuel Cost " << setw(34) << "[SEK/Mile] " << setw(3) << ": " << setw(3) << "" << fixed << setprecision(2) << fuel_cost << endl << endl;

}

int main() {

    clear_console();

    render_starting_message();

    render_output();

    return 0;

}
