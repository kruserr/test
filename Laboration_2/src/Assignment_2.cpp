#include <iostream> // Standard
#include <iomanip> // For setw

using namespace std; // Standard

void render_starting_message();
void claculate_ascii_table(int selector);
void clear_console();
void render_ascii();

// Output starting message
void render_starting_message() {

    cout << "_________________________" << endl << endl;
    cout << "Assignment 2: ASCII-table" << endl;
    cout << "_________________________" << endl << endl;

}

// Output and process the ascii table
void claculate_ascii_table(int selector) {

    if( selector == 128 ) {

        int ascii_max = 128;
        char array_char[ascii_max];
        int array_int[ascii_max];

        // Create the arrays containing 32 to ascii_max
        for ( int array_iterator = 32; array_iterator < ascii_max; array_iterator++ ) {

            array_int[array_iterator] = array_iterator;
            array_char[array_iterator] = array_iterator;

        }

        // Since 128-32 = 96, and 16|96 = 6 times, we use 16 rows over 6 coloumns, ( row_iterator >= 32 && row_iterator < 48 ) equals to 16 iterations

        // Create the 16 rows
        for ( int row_iterator = 32; row_iterator < 48; row_iterator++ ) {
            
            // Create the 6 coloumns
            for ( int coloumn_iterator = 0; coloumn_iterator < 6; coloumn_iterator++ ) {

                // Add in total 16 entries for each coloumn
                cout << setw(5) << array_int[row_iterator + (16 * coloumn_iterator)] << setw(2) << array_char[row_iterator + (16 * coloumn_iterator)] << setw(5);

            }

            // Add new line after each coloumn
            cout << endl;

        }

        cout << endl;

    } else if( selector == 256 ) {

        int ascii_max = 256;
        char array_char[ascii_max];
        int array_int[ascii_max];

        // Create the arrays containing 32 to ascii_max
        for ( int array_iterator = 32; array_iterator < ascii_max; array_iterator++ ) {

            array_int[array_iterator] = array_iterator;
            array_char[array_iterator] = array_iterator;

        }

        // Since 256-32 = 224, and 16|224 = 14 times, we use 16 rows over 14 coloumns ( row_iterator >= 32 && row_iterator < 48 ) equals to 16 iterations

        // Create the 16 rows
        for ( int row_iterator = 32; row_iterator < 48; row_iterator++ ) {
            
            // Create the 14 coloumns
            for ( int coloumn_iterator = 0; coloumn_iterator < 14; coloumn_iterator++ ) {

                // Add in total 16 entries for each coloumn
                if( row_iterator + (16 * coloumn_iterator) != 127 ) {

                    cout << setw(5) << array_int[row_iterator + (16 * coloumn_iterator)] << setw(2) << array_char[row_iterator + (16 * coloumn_iterator)] << setw(5);

                } else if( row_iterator + (16 * coloumn_iterator) == 127 ) {

                    cout << setw(5) << array_int[row_iterator + (16 * coloumn_iterator)] << setw(3) << array_char[row_iterator + (16 * coloumn_iterator)] << setw(5);

                }

            }

            // Add new line after each coloumn
            cout << endl;

        }

        cout << endl;

    } else {

        cout << "You entered an invalid selector: " << selector << endl << "In claculate_ascii_table(), use either 128 for default ascii, or 256 for extended ascii" << endl;

    }

}

// Cross Platform clear console and default extended ascii for windows else normal ascii
#ifdef unix

void clear_console() {

    system("clear");

}

void render_ascii() {

    claculate_ascii_table(128);

}

#elif _WIN32

void clear_console() {

    system("cls");

}

void render_ascii() {

    claculate_ascii_table(256);

}

#else

void clear_console() { }

void render_ascii() {

    claculate_ascii_table(128);

}

#endif

int main() {

    clear_console();

    render_starting_message();

    render_ascii();
    
    return 0;

}
