FROM fedora:latest

RUN dnf -y update
RUN dnf -y install make cmake clang gcc gcc-c++ ninja-build findutils
