#include <iostream> // Standard
#include <string> // For strings

using namespace std; // Standard

int main() {

    // Cross platform clear console
    #ifdef unix

        system("clear");

    #elif _WIN32

        system("cls");

    #else

    #endif

    // Print starting message
    cout << "__________________________" << endl << endl;
    cout << "Assignment 2: Sort 3 names" << endl;
    cout << "__________________________" << endl << endl;

    // Define strings
    string temp_full_name, temp_first_name, temp_last_name, full_name1, first_name1, last_name1, full_name2, first_name2, last_name2, full_name3, first_name3, last_name3;

    // Define chars
    char start = 65, end = 122, first_name_start, last_name_start;

    // Loop 3 times
    for( int i = 1; i <= 3; i++ ) {

        int init_num = 1;

        // Do while input is not good
        do {

            cout << "Enter full name " << i << ": " << endl;

            getline(cin, temp_full_name);

            cout << endl;

            first_name_start = temp_full_name.at(0);

            size_t found = temp_full_name.find(" ") + 1;

            last_name_start = temp_full_name.at(found);

            // Validate user input
            if( first_name_start >= start && first_name_start <= end && last_name_start >= start && last_name_start <= end && found != 0 ) {

                temp_first_name = temp_full_name.substr(0, found);

                temp_last_name = temp_full_name.substr(found, string::npos);

                init_num = 0;

            } else {

                cout << "Invalid input" << endl;

            }

        // Escape while loop when user enters valid input
        } while ( init_num == 1 );

        // Assign temp data to current i data
        switch(i) {

            case 1:

                full_name1 = temp_full_name;
                first_name1 = temp_first_name;
                last_name1 = temp_last_name;

                break;

            case 2:

                full_name2 = temp_full_name;
                first_name2 = temp_first_name;
                last_name2 = temp_last_name;

                break;

            case 3:

                full_name3 = temp_full_name;
                first_name3 = temp_first_name;
                last_name3 = temp_last_name;

                break;

        }

    }

    // Print output
    if( full_name1 > full_name2 && full_name2 > full_name3 ) {

        cout << full_name3 << endl << full_name2 << endl << full_name1 << endl << endl;
	
    } else if( first_name1.at(0) > first_name3.at(0) && first_name3.at(0) > first_name2.at(0) ) {

		cout << full_name2 << endl << full_name3 << endl << full_name1 << endl << endl;
	
    } else if( full_name2 > full_name1 && full_name1 > full_name3 ) {

		cout << full_name3 << endl << full_name1 << endl << full_name2 << endl << endl;
	
    } else if( full_name2 > full_name3 && full_name3 > full_name1 ) {

		cout << full_name1 << endl << full_name3 << endl << full_name2 << endl << endl;
	
    } else if( full_name3 > full_name1 && full_name1 > full_name2 ) {

		cout << full_name2 << endl << full_name1 << endl << full_name3 << endl << endl;
	
    } else {

		cout << full_name1 << endl << full_name2 << endl << full_name3 << endl << endl;
	
    }

    return 0;

}
