#include <iostream> // Standard
#include <iomanip> // For setw
#include <string> // For strings
#include <algorithm> // For all_of
#include <tuple> // For tuple

using namespace std; // Standard

void clear_console();
void render_starting_message();
int user_input();
int validate_user_input();
tuple<int, int> reverse_user_input();
tuple<bool, int, int> process_user_input();
void render_output();
int navigation_validate_user_input();
void render_navigation();
void navigation();

// Cross Platform clear console
#ifdef unix

void clear_console() {

    system("clear");

}

#elif _WIN32

void clear_console() {

    system("cls");

}

#else

void clear_console() { }

#endif

// Output starting message
void render_starting_message() {

    cout << "________________________" << endl << endl;
    cout << "Assignment 3: Palindrome" << endl;
    cout << "________________________" << endl << endl;

}

// Returns user input if user entered a 5 digit number, else return 0
int user_input() {

    string input_number;

    cout << "Enter a 5 digit number: ";
    cin >> input_number;
    cout << endl;

    if( input_number.length() == 5 && all_of(input_number.begin(), input_number.end(), ::isdigit) ) {

        if ( stoi(input_number) > 10000 && stoi(input_number) < 100000 ) {

            return stoi(input_number);

        } else {

            return 0;

        }

    } else if( input_number.length() != 5 ) {

        return 0;

    }

    return 0;

}

// Return user input if input is valid
int validate_user_input() {

    int input_number = 0;

    while( input_number == 0 ) {

        input_number = user_input();

    }

    return input_number;

}

// Returns user input and reversed user input
tuple<int, int> reverse_user_input() {

    int remainder = 0;
    int reversed_number = 0;
    int input_number = validate_user_input();
    int temp_input_number = input_number;

    while( temp_input_number != 0 ) {

        remainder = temp_input_number % 10;

        reversed_number = reversed_number * 10 + remainder;

        temp_input_number /= 10;

    }

    return { input_number, reversed_number };

}

// Returns is_palindrome and user input and reversed user input
tuple<bool, int, int> process_user_input() {

    int temp_number = 0;
    int input_number, reversed_number;

    tie(input_number, reversed_number) = reverse_user_input();

    int temp_input_number = input_number;

    while( temp_input_number > temp_number ) {

        temp_number = temp_number * 10 + temp_input_number % 10;
        temp_input_number = temp_input_number / 10;

    }

    if( temp_input_number == temp_number || temp_input_number == temp_number / 10 ) {

        return { true, input_number, reversed_number };

    } else {

        return { false, input_number, reversed_number };

    }

    return { false, input_number, reversed_number };

}

// Output the result to the screen!
void render_output() {

    bool is_palindrome;
    int input_number, reversed_number;

    tie(is_palindrome, input_number, reversed_number) = process_user_input();

    cout << "Input number" << setw(12) << ": " << input_number << endl << "Reversed number" << setw(9) << ": " << reversed_number << endl << endl;

    if( is_palindrome == true ) {

        cout << input_number << " is a palindrome" << endl << endl;

    } else if( is_palindrome == false ) {

        cout << input_number << " is not a palindrome" << endl << endl;

    }

}

// Returns user input if user entered a 1 digit number, else return -1
int navigation_validate_user_input() {

    string input_number;

    cout << "";
    cin >> input_number;
    cout << endl;

    if( input_number.length() == 1 && all_of(input_number.begin(), input_number.end(), ::isdigit) ) {

        return stoi(input_number);

    } else if( input_number.length() != 1 ) {

        return -1;

    }

    return -1;

}

// Output the navigation menu to the screen
void render_navigation() {

    cout << "1 - Run Palindrome Calculator" << endl;
    cout << "0 - Exit" << endl << endl;

}

// Output and process navigation
void navigation() {

    int navigation_user_input = 1;

    while( navigation_user_input != 0 ) {

        render_navigation();

        int navigation_user_input = navigation_validate_user_input();

        if ( navigation_user_input == 1 ) {

            clear_console();

            render_starting_message();

            render_output();

        } else if ( navigation_user_input == 0 ) {

            break;

        } else {

            clear_console();

            render_starting_message();

        }

    }

}

int main() {

    clear_console();

    render_starting_message();

    navigation();

    return 0;

}
