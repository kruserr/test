#include <iostream> // Standard
#include <iomanip> // For setw
#include <tuple> // For tuple

using namespace std; // Standard

void clear_console();
void render_starting_message();
void render_hypotenuse_output(int katet_a, int katet_b, int hypotenuse_c);
tuple<int, int> claculate_pythagorean_triangles();
void render_output();

// Cross Platform clear console
#ifdef unix

void clear_console() {

    system("clear");

}

#elif _WIN32

void clear_console() {

    system("cls");

}

#else

void clear_console() { }

#endif

// Output starting message
void render_starting_message() {

    cout << "___________________________________" << endl << endl;
    cout << "Assignment 1: Pythagorean triangles" << endl;
    cout << "___________________________________" << endl << endl;

}

// Output the side lengths of triangles with a hypotenuse equal to 100, 200, 300, 400 and 500!
void render_hypotenuse_output(int katet_a, int katet_b, int hypotenuse_c) {

    switch ( hypotenuse_c ) {

        case 100:

            cout << setw(25) << "Hypotenuse = 100: " << "(" << katet_a << ", " << katet_b << ", " << hypotenuse_c << ")" << endl;
            break;

        case 200:

            cout << setw(25) << "Hypotenuse = 200: " << "(" << katet_a << ", " << katet_b << ", " << hypotenuse_c << ")" << endl;
            break;

        case 300:

            cout << setw(25) << "Hypotenuse = 300: " << "(" << katet_a << ", " << katet_b << ", " << hypotenuse_c << ")" << endl;
            break;

        case 400:

            cout << setw(25) << "Hypotenuse = 400: " << "(" << katet_a << ", " << katet_b << ", " << hypotenuse_c << ")" << endl;
            break;
        
        case 500:
            
            cout << setw(25) << "Hypotenuse = 500: " << "(" << katet_a << ", " << katet_b << ", " << hypotenuse_c << ")" << endl;
            break;

    }

}

// Calculate the amount of Pythagorean triangles
tuple<int, int> claculate_pythagorean_triangles() {

    int triangles_counter = 0;

    // No side of the triangle may exceed the length of 500!
    int hypotenuse_c_max = 500;

    // Loop from a = 1 to c_max - 2
    for ( int katet_a = 1; katet_a <= hypotenuse_c_max - 2; katet_a++ ) {
        
        // For every a, loop from b = 2 to c_max - 1
        for ( int katet_b = katet_a + 1; katet_b <= hypotenuse_c_max - 1; katet_b++ ) {
            
            // For every b, loop from c = 3 to c_max
            for ( int hypotenuse_c = katet_b + 1; hypotenuse_c <= hypotenuse_c_max; hypotenuse_c++ ) {
                
                // If generated triangle equals a pythagorean triangle
                if ( katet_a * katet_a + katet_b * katet_b == hypotenuse_c * hypotenuse_c ) {
                    
                    triangles_counter++;
                    
                    // Output the side lengths of triangles with a hypotenuse equal to 100, 200, 300, 400 and 500!
                    render_hypotenuse_output(katet_a, katet_b, hypotenuse_c);

                }

            }

        }

    }

    return { triangles_counter, hypotenuse_c_max };

}

// Output the result to the screen!
void render_output() {

    int triangles_counter, hypotenuse_c_max;

    tie(triangles_counter, hypotenuse_c_max) = claculate_pythagorean_triangles();

    cout << endl << "There are " << triangles_counter << " pythagorean triangles, where the hypotenuse is no more than " << hypotenuse_c_max << endl << endl;

}

int main() {

    clear_console();

    render_starting_message();

    render_output();
    
    return 0;

}
