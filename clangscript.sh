#!/bin/bash

find $WORKSPACE -name "*.cpp" -exec clang++ -Weverything -o clangscript.out "{}" \;
rm clangscript.out
