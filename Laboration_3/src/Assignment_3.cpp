#include <iostream> // Standard
#include <string> // For strings

using namespace std; // Standard

void clear_console();
void render_starting_message();
string user_input();
string encrypt_user_input(string encrypt_string);
string decrypt_user_input(string decrypt_string);
void render_output();

// Cross Platform clear console
#ifdef unix

void clear_console() {

    system("clear");

}

#elif _WIN32

void clear_console() {

    system("cls");

}

#else

void clear_console() { }

#endif

// Output starting message
void render_starting_message() {

    cout << "________________________" << endl << endl;
    cout << "Assignment 3: Encryption" << endl;
    cout << "________________________" << endl << endl;

}

// Return user input string
string user_input() {

    string user_input;

    // Let the user input a text. All characters are allowed!
	cout << "Input: ";
	getline(cin, user_input);
    cout << endl;

    return user_input;

}

// Return encrypted user input string
string encrypt_user_input(string encrypt_string) {

    // Save the encrypted text in a separate string!
	string output = encrypt_string;

    int size_of_string = encrypt_string.size();

    // Use two encryption keys rot13 and rot7!
	for (int i = 0; i < size_of_string; i++) {

		output[i] = encrypt_string[i] + 7;

        // Alternate key for each fifth character!
		if (i % 5 == 0) {

			output[i] = encrypt_string[i] + 13;

		}

	}

    // Original input string gets destroyed when leaving this scope
	return output;

}

// Return decrypted user input string
string decrypt_user_input(string decrypt_string) {

	string output = decrypt_string;

    int size_of_string = decrypt_string.size();

    // Use keys as presented above!
	for (int i = 0; i < size_of_string; i++) {

		output[i] = decrypt_string[i] - 7;

		if (i % 5 == 0) {

			output[i] = decrypt_string[i] - 13;
			
		}

	}

	return output;

}

// Print result to the screen
void render_output() {

    // Save the encrypted text in a separate string!
    string encrypted_string = encrypt_user_input( user_input() );

    // Print the encrypted string to the screen!
	cout << "Encrypted: " << encrypted_string << endl;

    // Print the decrypted string to the screen!
	cout << "Decrypted: " << decrypt_user_input(encrypted_string) << endl << endl;

}

int main() {

    clear_console();

    render_starting_message();

    render_output();

    return 0;

}
