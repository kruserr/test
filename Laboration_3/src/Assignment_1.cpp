#include <iostream> // Standard
#include <string> // For strings

using namespace std; // Standard

void clear_console();
void render_starting_message();
string define_strings();
string process_strings();
void render_output();

// Cross Platform clear console
#ifdef unix

void clear_console() {

    system("clear");

}

#elif _WIN32

void clear_console() {

    system("cls");

}

#else

void clear_console() { }

#endif

// Output starting message
void render_starting_message() {

    cout << "________________________________________" << endl << endl;
    cout << "Assignment 1: Replacing words in strings" << endl;
    cout << "________________________________________" << endl << endl;

}

// Return all strings in one string
string define_strings() {

    string const inText1 = "Foten är en kroppsdel som förekommer mycket i våra uttryck.";

    string const inText2 = "På stående fot betyder omedelbart, utan förberedelse.";

    string const inText3 = "Försätta på fri fot betyder att ge någon friheten.";

    string const inText4 = "Sätta foten i munnen betyder att göra bort sig.";

    string const inText5 = "Få om bakfoten betyder att missuppfatta något.";

    string const inText6 = "Skrapa med foten betyder att visa sig underdånig eller ödmjuk.";

    string const inText7 = "Stryka på foten betyder att tvingas ge upp.";

    string const inText8 = "Leva på stor fot betyder att föra ett dyrbart eller slösaktigt leverne.";
    
    string const inText9 = "Varför fick du foten???";

    // Merge the string constants into a single string!
    return inText1 + inText2 + inText3 + inText4 + inText5 + inText6 + inText7 + inText8 + inText9;

}

// Return processed strings
string process_strings() {

    string all_strings = define_strings();

    // It should not matter whether the word fot occurs as upper or lower case, nor if it’s part of a composition such as foten!
    // Each new sentence must begin with an upper-case character!
    while (all_strings.find("Foten") != string::npos) {

        all_strings.replace(all_strings.find("Foten"), 3, "Hand");

	}

    // It should not matter whether the word fot occurs as upper or lower case, nor if it’s part of a composition such as foten!
	while (all_strings.find("fot") != string::npos) {
		
        all_strings.replace(all_strings.find("fot"), 3, "hand");

	}

    // It should not matter whether the word fot occurs as upper or lower case, nor if it’s part of a composition such as foten!
	while (all_strings.find("foten") != string::npos) {

		all_strings.replace(all_strings.find("foten"), 3, "hand");

	}

    // Insert line breaks after each punctuation!
	while (all_strings.find(".") != string::npos) {

		all_strings.replace(all_strings.find("."), 1, ",\n");

	}

    // Insert line breaks after each punctuation!
    while (all_strings.find(",") != string::npos) {

		all_strings.replace(all_strings.find(","), 1, ".");

	}
    
    return all_strings;

}

// Output result to the screen
void render_output() {

    // Print manipulated string to the screen!
    cout << process_strings() << endl << endl;

}

int main() {

    clear_console();

    render_starting_message();

    render_output();

    return 0;

}
